# Class Assignment 3 Report

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

## 1.1 **CA3-part2**

### 1.1.1 ***Setup***

We can start by cloning or copy/pasting the react-and-spring-dat-rest-basic folder with the CA2/part2 exercise to the CA3/part2 
directory, in order to not change the original CA2/part2 exercise.

Don't forget to make the repository public:
![img.png](img.png)

We will be using a VagrantFile example from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/, so we have to 
copy/paste it to our CA3/part2 directory as well.

Personal NOTE:

It seems that Vagrant does not handle well when you have a Windows Username with special characters 
(such as : ~ , ´ , etc). In my case my Username is João. Therefore I had to create a new User : "Devops", and work in 
that Windows User.
![img_1.png](img_1.png)


### 1.1.2 ***Vagrant File needed Update***

We are using Java 11 with our application. Since the envimation/ubuntu-xenial box, from the provided Vagrantfile example,
does not support it, we will need to adapt the vagrant file for a different box. We chose the bento/ubuntu-18.04.

Therefore we need to change the provision section on the VagrantFile:

We added a command to clean our repository before we clone it... since we will be experimenting a lot.

      rm -R -f devops-21-22-atb-1211774

Then a link to clone our repository:
      
      git clone https://JoaoSa1211774@bitbucket.org/joaosa1211774/devops-21-22-atb-1211774.git

Then, we instruct the shell to change to the directory where the gradle version of the spring boot application is located:

      cd devops-21-22-atb-1211774/CA3/part2/react-and-spring-dat-rest-basic

We have to give execution permission to the gradle wrapper file:

      chmod u+x gradlew

Lastly we can give instruction for the gradlew clean build:

      ./gradlew clean build

### 1.1.3 ***Application needed Updates***

As said before, since we have the Java 11 version, we have to change the front-end plugins, source-compatibility config
and on the assembleScript we changed the  'run build' to 'run webpack',   on our build.gradle, so we don't have compatibility
issues... After some desperate experimenting and research this is the final result for my specific case (that works):

![img_7.png](img_7.png)

Since the previous application generated .jar files, and now we want to generate .war files, in order for our app to be 
deployed on the Tomcat8 server, (on a first instance) we must add the .war plugin  on build.gradle:

      id war

...and the dependency wit providedRuntime Scope.

      providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

After Adding the ServletInitializer to our application, on the VagrantFile, after the command ./gradlew clean build 
(that builds the app) , we can add the command to copy the built .war file with the name
"react-and-spring-dat-rest-basic-0.0.1-SNAPSHOT.war" to the Tomcat8 server folder:

    sudo cp ./build/libs/react-and-spring-dat-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

We also added the "webpack": "webpack"  key/value pair on our package.json file, and removed the other: "build,check, cleant, etc", because it were not needed for it to work:

![img_8.png](img_8.png)

For the h2 console configuration  we added some lines to the application.properties file. 

We added the

    "spring.h2.console.settings.web-allow-others=true" 
to enable the possibility of connection to a remote
database.

We added as well, the path to the .war file with

    "server.servlet.context-path=/basic-0.0.1-SNAPSHOT" 
in the beginning of the 
application.proprieties.

Also, we have changed the 

    "spring.datasource.url=jdbc:h2:mem:jpadb"    
with    
    
    "spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE"
, in order to 
specify the path to the remote h2 database, instead of the in-memory h2 database.

Because the database will drop every time the application is executed, we need to add: 
    
    spring.jpa.hibernate.ddl-auto=update

... after

    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

...and before

    spring.h2.console.enabled=true


This is the end result for the application.properties file:
![img_9.png](img_9.png)


We need to change the path (for the .war file) where the GET request is to be sent. This can be done on the app.js file:

![img_10.png](img_10.png)


On the index.html file, we should correct the CSS stylesheet href output to the correct one: "main.css"

![img_11.png](img_11.png)



On a terminal you now can run:

    vagrant up --provision

... and the final Result will be:

![img_4.png](img_4.png)
