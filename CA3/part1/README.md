# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

## 1.1 **CA3-part1**

### 1.1.1 **CA3-part1 (CA1)**

Firstly we should Create and Setup a VirtualMachine by using (for example) Virtual Box.


On this example we used Ubuntu as the OS.

Following these settings:

    1 Create VM
    2 Change VM settings:
        Connect image (ISO) with the
        Ubuntu 18.04 minimal installation
        media: https:
        //help.ubuntu.com/community/
        Installation/MinimalCD
    3 2048 MB RAM
    4 Set Network Adapter 1 as Nat
    5 Set Network Adapter 2 as Host-only Adapter - with ipv4 address 192.168.56.5
    6 Start the VM and install Ubuntu

As said before the adapter 2 (Host-Only adapter) in this case was configured with ipv4 address 192.168.56.5.

Then we should follow these instructions:

    Update the packages repositories
        sudo apt update
    Install the network tools:
        sudo apt install net-tools
    Edit the network configuration file to setup the IP
        sudo nano /etc/netplan/01-netcfg.yaml
    Apply the new changes:
        sudo netplan apply
    Install openssh-server so that we can use ssh to open secure terminal sessions to the VM (from other hosts)
        sudo apt install openssh-server
    Enable password authentication for ssh
        sudo nano /etc/ssh/sshd_config
        uncomment the line PasswordAuthentication yes
        sudo service ssh restart
    Install an ftp server so that we can use the FTP protocol to transfers files to/from the VM (from other hosts)
        sudo apt install vsftpd
    Enable write access for vsftpd
        sudo nano /etc/vsftpd.conf
        uncomment the line write_enable=YES
        sudo service vsftpd restart
    Since the SSH server is enabled in the VM we can now use ssh to connect to the VM.
    In the host (Windows or Linux or OSX), in a terminal/console, type:
        ssh joao@192.168.56.5
    Where joao should be replaced by the user name of the VM
    Where 192.168.56.5 should be replaced by the IP of the VM

(In my case the VM username is joao as well - since it is my name)

Since the FTP server is enabled in the VM we can now use an ftp application, like FileZilla, to transfer files to/from the VM.

Next we should install Git and jdk.

        sudo apt install git

        sudo apt install openjdk-8-jdk-headless

After that we can clone our Repository with git clone... This time on our VM terminal:

    git clone git@bitbucket.org:joaosa1211774/devops-21-22-atb-1211774.git

![img.png](img.png)

Then change the directory to the /basic directory and try to spring-boot run it with maven:
![img_1.png](img_1.png)

...as you can see we have no permission to run the mvmw file. Therefore we have to give executing permissions. And then we can successfully  :

![img_2.png](img_2.png)

On our Browser we can access the 192.168.56.5:8080 on our real machine : and everything works fine:

![img_3.png](img_3.png)

### 1.1.2 **CA3-part1 (CA2 -part1)**

For the Ca2 part 2 , we must navigate to the /gradle_basic_demo directory and build it with: ./gradlew build

![img_5.png](img_5.png)

Again we have to give executing permissions.

![img_6.png](img_6.png)

Then we can build it with no problems:
![img_7.png](img_7.png)
![img_8.png](img_8.png)

Still on th VM we can the runServer task:
![img_9.png](img_9.png)

Since we have the open ssh feature enabled, we can run the task runClient (3) on a Powershell on our Local/physical machine (firstly we need to login on it (1) and build gradle (2) ) :

(1)
![img_11.png](img_11.png)
(2)
![img_12.png](img_12.png)
(3)
![img_14.png](img_14.png)

        NOTE:
        
        We have to change the args from "LocalHost" to the chosen ipv4 :   192.168.56.5
![img_13.png](img_13.png)


Even thought everything was made correctly t, since the Ubuntu (we are running on our VM) does not have a graphical 
Interface, the task will fail.
![img_15.png](img_15.png)


### 1.1.2 **CA3-part1 (CA2 -part2)**

First we will again change to the react-and-spring-dat-rest-basic directory and build Gradle:
![img_16.png](img_16.png)

...again we need to give executing permissions:
![img_17.png](img_17.png)
![img_18.png](img_18.png)

after successfully building we can run it with ./gradlew bootrun, everything must work fine :
![img_19.png](img_19.png)

... and the results must show correctly:
![img_21.png](img_21.png)

As a Bonus :  We can also run the Copy task already created:
![img_22.png](img_22.png)