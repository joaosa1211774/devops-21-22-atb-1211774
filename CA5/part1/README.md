# Class Assignment 5 Report

## 1. Analysis, Design and Implementation

## 1.1 **CA5-part1**

### 1.1.1 ***Main Assignment***

Firstly we should Download and install Jenkins. In my case I have chosen the Long Term Support version.

NOTES:
* We should track the place to where the MSI file is downloaded.
* We can use the Jenkins Documentations to help us to install it:  https://www.jenkins.io/doc/book/installing/windows/#invalid-service-logon-credentials

After installing it with the wizard, we can run it on our localhost:8080:
![img.png](img.png)


We must find the Jenkins password and apply it.
![img_1.png](img_1.png)

check if (for now) the Pipeline plug-in is marked to be installed:
![img_2.png](img_2.png)


![img_3.png](img_3.png)

We have to now create a pipeline:
![img_4.png](img_4.png)

and we can chose either running the script directly in Jenkins or direct it our Jenkinsfile in our repo (as I did in my case): 
![img_5.png](img_5.png)
![img_10.png](img_10.png)

This is the scripts for the pipeline, in my Jenkinsfile. In my case I had to specify the 
the branch "main" where I was working, because I am not working in the master branch:


        pipeline {
        agent any

        stages {
        stage('Checkout') {
        steps {
        echo 'Checking out...'
        git branch:'main', url: 'https://bitbucket.org/joaosa1211774/devops-21-22-atb-1211774'
        }
        }
                stage('Assemble') {
                steps{
                echo 'Assembling...'
                dir ("CA2/Part1/gradle_basic_demo"){
                echo 'estou aqui'
        bat './gradlew assemble'
                }
                }
                }
                stage('Test'){
                steps {
                echo 'Testing...'
                dir ("CA2/Part1/gradle_basic_demo"){
                bat './gradlew test'
                junit 'build/test-results/test/*.xml'
                }
                }
                }
        stage('Archiving'){
        steps{
        echo 'Archiving...'
        dir ("CA2/Part1/gradle_basic_demo"){
        archiveArtifacts 'build/distributions/*'
        }
        }
        }
        }
        }

As we can see on the stageView, everything is working:

![img_9.png](img_9.png)


As we can see the tests have passed:
![img_8.png](img_8.png)


