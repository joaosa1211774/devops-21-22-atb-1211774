package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    public void createEmployee_withNullFirstName(){
        //arrange
        String firstName = null;
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withEmptyFirstName(){
        //arrange
        String firstName = "   ";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withNullLastName(){
        //arrange
        String firstName = "joao";
        String lastName = null;
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withEmptyLastName(){
        //arrange
        String firstName = "joao";
        String lastName = "  ";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withNullDescriptionName(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = null;
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withEmptyDescriptionName(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "  ";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }

    @Test
    public void createEmployee_withNullJobTitle(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = null;
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear,email);});
    }

    @Test
    public void createEmployee_withEmptyJobTitle(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "   ";
        int jobYear = 2;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }
    @Test
    public void createEmployee_withNegativeJobYear(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = -1;
        String email = "email@email.com";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }
    @Test
    public void createEmployee_withEmptyEmail(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 1;
        String email = "";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }
    @Test
    public void createEmployee_withNullEmail(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = null;
        //act
        //assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }
    @Test
    public void createEmployee_withEmailWithoutTheAtSign(){
        //arrange
        String firstName = "joao";
        String lastName = "sa";
        String description = "best";
        String jobTitle = "boss";
        int jobYear = 2;
        String email = "email";
        //act
        //assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYear, email);});
    }
}