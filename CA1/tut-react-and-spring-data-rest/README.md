# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*On the Repository Folder Directory, after cloning the source code on the created folder Ca1, open IDE.*

*On a Terminal: Go to the repository folder directory - using:  -cd ....*

        git commit -a -m "initial commit" 
- this will be the first commit and will also add all untracked files

create a tag "v1.1.0" with : 

        git tag "v1.1.0"

push these changes to the main branch 

     git push origin main

push the tags to associate with the previous 

        git push origin --tags

Create an issue #1 for example: "Ca1 FirstWeek"

*after doing the necessary modifications and tests on the code, commit the changes and push them as well:*

Firstly create the necessary tags

        git tag "v1.2.0"

        git tag "ca1-part1"

On the commit text, make sure the text "Close #1"  is added to the Commit text, in order to automatically close the issue #1. For example: 

        git commit -a -m "..... - Close #1"

Push the committed changes:

        git push origin main

And push the created tags 

        git push origin --tags


Create the New issue #2  "Ca1 SecondWeek"

Create the new "email-field" branch

        git branch "email-field"

Checkout on that branch using:

        git checkout "email-field"

Then proceed with necessary changes and tests in order to support the feature. 

After that create the tag:

        git tag "v1.3.0"

Commit the changes:

        git commit -a -m "..."

...push the changes on the branch "email-field":

        git push origin "email-field"


after pushing the branch changes, chang the working branch to the main using:

        git checkout main

merge the current branch with the "email-field":

        git merge "email-field"

add the tags previously created

        git push origin --tags

Create the new "fix-invalid-email" branch

        git branch "fix-invalid-email"

Checkout on that branch using:

        git checkout "fix-invalid-email"

Then proceed with necessary changes and tests in order to support the feature. 

After that create the tags:

        git tag "v1.3.1"
        git tag "ca1-part2"

Commit the changes with the keyword Close #2 in order close the issue:

        git commit -a -m "add feature to validate an invalid email - Closes #2"

...push the changes on the branch "fix-invalid-email":

        git push origin "fix-invalid-email"


after pushing the branch changes, chang the working branch to the main using:

        git checkout main

merge the current branch with the "fix-invalid-email":

        git merge "fix-invalid-email"

add the tags previously created

        git push origin --tags



## 2. Analysis of an Alternative

1 Present how the alternative tool compares to Git regarding version control features;
2 Describe how the alternative tool could be used (i.e., only the design of the solution)
to solve the same goals as presented for this assignment (see previous 2 slides);


*For this assignment there are several alternative tools for Git.

One of these Distributed Version Control System alternatives is Mercurial.

Mercurial is free and open-source, same as Git. 
 
One possible repository in alternative to bitbucket is the HelixTeamHub Repository, that can host Mercurial.

Since Git is more popular than Mercurial, most of the code-sources are shared in Git. 

Therefore, some additional steps are needed in order to convert the files from git to Mercurial. (these are shown below).

Also, Mercurial does not have a functionality to close an Issue with the commit message, in opposition to git.

in Overall the syntax is pretty similar to Git, with some minor changes.

For example in Mercurial everything we do is essentially a commit. Which is sometimes a bit confusing


 # 3. Implementation of the Alternative

Firstly, Mercurial must be installed and both it and HelixTeamHub should be set up.

1) We can create the HelixTeamHub repository and anchor it in a  C: folder. In my case I have cd to the C:\projectSwitchDEVOPS\devops-21-22-atb-1211774_alternaticaMercurial\Alternative. 
-cd C:\projectSwitchDEVOPS\devops-21-22-atb-1211774_alternaticaMercurial\Alternative ,  and then cloned the repository:

      hg clone https://1211774isepipppt@helixteamhub.cloud/institutosuperiorengenhariadoporto-politcnicodoporto/projects/devops-21-22-atb121177_mercurial_helix_alternative_/repositories/mercurial/devops_gitAlternative  


Then (as said above) we need to convert this Git file repository to Mercurial:

2) clone the initial git source-file to a local folder. https://github.com/spring-guides/tut-react-and-spring-data-rest.git -> because the conversion does not work with a remote file
3)search for the mercurial.ini file

![img.png](img.png)

   1) add the following text to the file and save it:

       [extensions]
      hgext.convert=
   
   2) use the cmd: hg convert --datesort <sourcefile> <destination> to convert the repository:
      1) in my case:  hg convert C:\projectSwitchDEVOPS\devops-21-22-atb-1211774_alternaticaMercurial\tut-react-and-spring-data-rest C:\projectSwitchDEVOPS\devops-21-22-atb-1211774_alternaticaMercurial\Alternative\devops_gitAlternative2
   
   3) then copy the tut-react-and-spring-data-rest folder to the new Alternative folder.
   4) then add the files with:  hg add .
   5) Commit 
      1) hg commit -m "inital commit"
   6) then push it:   
      1) hg push 
      2) then insert my Helix password
   7) Creating both part 1 and 2 issues on Helix. The issue creation UI is very USER-friendly.
      1) ![img_1.png](img_1.png)
         1) It is possible to associate the issues to a Milestone. in this case I have created a Milestone for the Ca1 both parts
            ![img_2.png](img_2.png)
      
   8) tag creation :   hg tag "v1.1.0"

   9) push the changes :  hg push

   10) make the necessary code exercise changes.

   11) Create both tags: 

         hg tag "v1.2.0"

         hg tag "ca1-part1"

   12) commit changes referencing/adressing the issue number with #1  :  hg commit -m "jobYears feature added and tested - close #1"

   13) we have to close the issue manually on the Helix Platform 

   14) push the changes and tags :  hg push
![img_3.png](img_3.png) 
![img_4.png](img_4.png)

   15) for the second part of the exercise we have to work on new branches. Therefore, we create a new branch with the "email-field" name.
       1) after that, commit in order to upload the branch 
            hg branch "email field"
           hg tag "v1.3.0"
               hg commit -m "email-field creation and testing - for email-field"
          hg push --new-branch
   16) do necessary changes
   17) then close the branch:  hg ci --close-branch -m "email feature is don
       e"

   18) change to the default branch : hg update default
   19) merge them :  hg merge "email-field"
   20) and commit : hg ci -m "merge email-field branch into defau
       lt"
   21) for the last part : create the new branch : fix-invalid-email
   - hg branch "fix-invalid-email"
   - hg tag "v1.3.1"
     hg commit -m "fix-invalid-email methods creation and testing - for fix-invalid-email"
     hg push --new-branch
       16) do necessary changes
       17) commit: hg commit -m "fix-invalid-email creation and testing- for fix-invalid-email"

17) then close the branch:  hg ci --close-branch -m "invalid email feature is don
    e"

18) change to the default branch : hg update default
19) merge them :  hg merge "fix-invalid-email"
20) and commit : hg ci -m "merge email-field branch into defau
    lt - closes #3"
21) hg push
    
