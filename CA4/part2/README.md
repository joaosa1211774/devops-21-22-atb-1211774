# Class Assignment 4 Report

## 1. Analysis, Design and Implementation

## 1.1 **CA4-part2**

### 1.1.1 ***Main Assignment***

Creating the web and db DockerFiles (from the repo https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/copied from the atb repository)  and the docker-compose.yml file.

#### db\Dockerfile :

For the DB Dockerfile, there is no change from the original file. Just the JDK installation version must be the JDK11.

![img_13.png](img_13.png)

#### Web\Dockerfile :

![img_9.png](img_9.png)

**...in detail:**

This is the image will be built from, since the JDK version is jdk11:

        FROM tomcat:9.0.48-jdk11-openjdk-slim

Dependencies installation:
        
        RUN apt-get update -y
        RUN apt-get install -f
        RUN apt-get install git -y
        RUN apt-get install nodejs -y
        RUN apt-get install npm -y

creating a temporary folder and clone our own repository into it:

        RUN mkdir -p /tmp/build

        WORKDIR /tmp/build/

        RUN git clone https://JoaoSa1211774@bitbucket.org/joaosa1211774/devops-21-22-atb-1211774.git

changing to the root folder, grant execute permissions over the build.gradle file, and build our application:

        WORKDIR /tmp/build/devops-21-22-atb-1211774/CA4/part2/react-and-spring-dat-rest-basic

        RUN chmod u+x gradlew
        RUN ./gradlew clean build

copying the built .war file into the Tomcat server folder:

        RUN cp build/libs/react-and-spring-dat-rest-basic-0.0.1-SNAPSHOT /usr/local/tomcat/webapps/

exposing Tomcat's default port, 8080 where our app will be running:

        EXPOSE 8080

#### docker-compose.yml :

![img_15.png](img_15.png)

**... in detail:**

There are the two services (web and db) being declared and on each one it is indicated the relative path to the directory where the Dockerfiles for building the images are located.

The network is created and the IP adderess's of the containers, are defined. This is in order for them to be connected in the same network.

The line "host-port:container-port" is needed to map the containers exposed ports to the host's ports.

The web container has a dependency on the db with the "depends_on" code, therefore the db container is always created first.
 
A directory in the container and a directory in the host whose contents are synchronized (VOLUME), are defined with: " host-dir:container-dir"

####Additional changes

Changing the IP of the db service on the application.properties of the react-and-spring-data-rest-basic from 192.168.56.11:9092 to 192.168.33.11:9092

![img_14.png](img_14.png)

#### DOCKER-COMPOSE

After these changes, in order to create the images, starting the containers and creating the network,  we can run the "docker-compose up" command on a terminal ... on the correct directory (obviously). 

![img.png](img.png)
![img_1.png](img_1.png)

![img_4.png](img_4.png)
![img_3.png](img_3.png)

As seen above the images were created, the containers are running, and the Networks was created.

If we access our application.... in my case with  "http://localhost:8080/react-and-spring-dat-rest-basic-0.0.1-SNAPSHOT/", we can see that it is working:

![img_2.png](img_2.png)

....and 
http://localhost:8080/react-and-spring-dat-rest-basic-0.0.1-SNAPSHOT/h2-console/login.jsp?jsessionid=0ca56444d05eac19e637a0e4bcfd35eb


![img_5.png](img_5.png)


###  get a copy of the database file

In order to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume copy, by using a volume with the db container, we will need to follow these steps:

While the container is up and running we have to access a shell session inside the container, and run the following command: "$ cp /usr/src/app/jpadb.mv.db /usr/src/data-backup "
![img_6.png](img_6.png)

As you can see the "data" directory was created and within is the database file.

![img_7.png](img_7.png)


### Images pushing

in order to push both images to DockerHub, we must:

Login with Docker Hub with our credentials:
![img_16.png](img_16.png)

Create Tags for both images:
![img_17.png](img_17.png)

And then, push them to the repository:
![img_10.png](img_10.png)

![img_11.png](img_11.png)

As you can see both of them are in DockerHub:

![img_12.png](img_12.png)


### 1.1.2 ***Alternative to Docker - Kubernetes***


#### Definition

Kubernetes is an open source system for automating the deployment, scaling, and management of containerized applications. Kubernetes is a container orchestration system and can be used with or without Docker.
It is considered the market leader and industry standard for container orchestration and distributed application deployment.
It can automatically orchestrate different containers on different physical or virtual servers based on small configurations. In other words, Kubernetes bundles a number of containers into a group that it manages on the same machine to reduce network overhead and increase resource utilisation efficiency.
Kubernetes is particularly useful for DevOps teams because it provides service discovery, load balancing within the cluster, automated rollouts and rollbacks, self-healing of containers that fail, and configuration management. In addition, Kubernetes is an important tool for building robust DevOps CI/CD pipelines.

#### Advantages
Automated operations: Kubernetes has a powerful API and a command-line tool called kubectl that can do
much of the heavy lifting in container management by allowing you to automate your operations.
Infrastructure abstraction: Kubernetes manages the resources made available to it on your behalf. This frees
Developers to focus on writing application code rather than the underlying compute, networking or storage infrastructure.
Service health monitoring: Kubernetes monitors the running environment and compares it to the desired state. It performs automatic health checks on services and restarts containers that have failed or been stopped.

#### Difference between Docker and Kubernetes?
While Docker is a container runtime environment, Kubernetes is a platform for running and managing containers from many container runtimes. Kubernetes supports many other container runtimes.
So when comparing the two, Kubernetes should be compared more to Docker Swarm. Docker Swarm is a container orchestration tool like Kubernetes, meaning it allows you to manage multiple containers deployed on multiple hosts with Docker Server.
Docker Swarm offers the same benefits as Kubernetes, such as deploying your application via declarative YAML files, automatically scaling services to the desired state, load balancing between containers within the cluster, and security and access control for your services.
If you run few workloads, don't need to worry about managing your own infrastructure, or don't need specific features of Kubernetes, Docker Swarm may be a good choice. If you run many workloads and need cloud-native interoperability and have many teams in your organization, requiring more isolation of services, Kubernetes is probably the platform to consider.