# Class Assignment 4 Report

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

## 1.1 **CA4-part1**

### 1.1.1 ***Version 1***

After installing Docker to Desktop, we can proceed in creating a dockerFile in our 
Class Assignment directory: CA4/part1.

![img_10.png](img_10.png)

Detailing each code line:

First we are indicating on which image the DockerFile will be based upon. In our case
it will be on a pre-existing image of Ubuntu (with that name in DockerHub).: 

        FROM ubuntu

...installation of java and git:

        RUN apt-get update -y
        RUN apt-get install -y openjdk-11-jdk-headless
        RUN apt-get install -y git

...changing to the /root directory:

        WORKDIR /root

...Cloning the repository:

        RUN git clone https://JoaoSa1211774@bitbucket.org/joaosa1211774/devops-21-22-atb-1211774.git

...changing to the directory where our gradle file is:

        WORKDIR devops-21-22-atb-1211774/CA2/Part1/gradle_basic_demo

...giving executing permissions to execute the gradlew file.  And build our application:

        RUN chmod u+x gradlew
        RUN ./gradlew clean build

...exposing the port of the container. (this is the same that is launched on the runServer task of the gradle file )
       
         EXPOSE 59001

... running the server:

        ENTRYPOINT ./gradlew runServer


After pushing the changes to our remote repository, we should change to the directory of the created Dockerfile,
and then we can build, in order to create the image from that dockerfile.

![img_8.png](img_8.png)

As we can see bellow, the image: my-image-joao6 was created.
 
![img_9.png](img_9.png)

Running the container

![img_6.png](img_6.png)

successfully created, running and working:

![img_7.png](img_7.png)

![img_16.png](img_16.png)

### 1.1.2 ***Version 2***

Firstly we need to compile the application in our host, (which we have previously made). Then we
can put the .jar file to the same directory of the Dockerfile.

After that we will, as for the previous version, create the dockerfile.

![img_17.png](img_17.png)

**Explaining the differences from the previous version:**

In this case we won't need to install git, we just need JDK-11, since we have the .jar for our dockerfile to copy it.

        RUN apt-get update -y
        RUN apt-get install -y openjdk-11-jdk-headless

...copying the jar file to inside the container:

        COPY basic_demo-0.1.0.jar basic_demo-0.1.0.jar

...we have  the Original-Project documentation command, so the container can run the server.

        ENTRYPOINT java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001


Building the image

![img_11.png](img_11.png)

...as seen bellow, this created image is half the size of the previous one as expected, since the 

![img_13.png](img_13.png)


running the container:

![img_12.png](img_12.png)

...success:

![img_14.png](img_14.png)

Running the on the Client(s) sides:

![img_15.png](img_15.png)


### 1.1.3 ***Pushing the Images to the Docker-hub Repository***

In order to push the images to our repository, we need to first  Login on Docker hub:

![img_18.png](img_18.png)

We need to tag them both using our DockerHub username (joaopedroserrasa in my case):

![img_20.png](img_20.png)
![img_21.png](img_21.png)

Pushing the version1 image:

![img_22.png](img_22.png)

As was expected, when pushing the v2 image the time was significantly less.

![img_23.png](img_23.png)

As we can see if we visit our repository in DockerHub, we will see both our images:

![img_24.png](img_24.png)