# Class Assignment 1 Report

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

*A section dedicated to the description of the analysis, design and
implementation of the requirements*

*Should follow a "tutorial" style (i.e., it should be possible to
reproduce the assignment by following the instructions in the
tutorial)*

* *Should include a description of the steps used to achieve the
  requirements*
* *Should include justifications for the options (when required)*

## 1.1 **CA2-part1**

After downloading and committing the example application to the repository, we can add the new task to execute the server.

This must be done on the build gradle file.

     "task runServer(type:JavaExec, dependsOn: classes){
     group = "DevOps"
     description = "Executes the Server Class"
 
     classpath = sourceSets.main.runtimeClasspath
 
     mainClass = 'basic_demo.ChatServerApp'
 
     args '59001'
     }"

Then I have added the following unit test on the proposed directory path src/test/java/basic_demo/AppTest.java.

    package basic_demo;
    import org.junit.Test;
    import static org.junit.Assert.*;
    public class AppTest {
    @Test public void testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
    }

Also adding the required junit dependency in gradle.

We should build first a .jar file with the application... on the terminal:

     ./gradlew build

then, in order to run the server, on a terminal, cd to the project root directory and then:

     java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

I have used the server port 59001 as proposed on the tutorial.

then I have used the following command on two different terminals:

     ./gradlew runClient

![img.png](img.png)

This is needed in order to test the app running.

+In order to add the new task of type copy, I can use the following code. 
*When choosing a non-existent destiny folder (in this case with the name backup), the folder will be created on the first time I run*
    
    task copy(type: Copy) {
    from 'src'
    into 'backup'
    }

for the task related to the zip file of the src, I have used the following code :
    
    task myZip(type: Zip) {
    //Directory to output ZIP file
    destinationDirectory = file('.') 
    //File name to archive
    archiveFileName = 'zip' + '.zip';
    from 'src' 
}

If I run both tasks, it is possible to see the folder 'backup' and the 'zip' zip file have been created:

![img.png](img.png)



 