# Class Assignment 3 Report

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

## 1.1 **CA2-part2**

First create and checkout the branch tut-basic-gradle

        git branch tut-basic-gradle
        git checkout tut-basic-gradle

On the Spring Initializer, chose the following configurations and dependencies:

![img.png](img.png)
 
than extract it to the Ca2 part 2 folder.

**![img_1.png](img_1.png)**

After that delete the src folder and copy the src folder from the tutorial:

        In my case I have cloned the tutorial again to an auxiliary folder, for personal organization purposes.
        Because of that I did not had the src/main/resources/static/built/ to delete.

Copy also the files webpack.config.js and package.json to the Ca2/part2 folder.

then add the following line on the plugin declaration on the build.gradle file:

    id "org.siouan.frontend-jdk11" version "6.0.0"
    **>>in my case I have used the jdk11>>>**

in order to configure the previous plugin, add also the following code to the build.gradle file:

    frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run build"
    cleanScript = "run clean"
    checkScript = "run check"
    }

after that update the scripts on the package.json, in order to configure the execution of the webpack:

*In my case I have changed the existent scripts:*

        "scripts": {
        "watch": "webpack --watch -d --output ./target/classes/static/built/bundle.js"
        },
    
*.....by:*
    
    "scripts": {
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",    
    "test": "echo Testing frontend"
    },


create a task to copy

    task copy(type: Copy) {
    from 'react-and-spring-dat-rest-basic/gradle/wrapper/gradle-wrapper.jar'
    into 'react-and-spring-dat-rest-basic/dist'
    }

create the task to delete the webpack generated files

    task delete(type: Delete){
     delete '/src/main/resources/static/built'
    }

since task delete must occur before the clean we have to create the dependency:

    clean.dependsOn delete

End